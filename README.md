Time Tracker
============

Track the hours you work.

Requirements
------------
* **Linux** (I'm using Ubuntu 12.04)
* Ruby 1.9+ (I recommend using `rvm` to install and use ruby on Linux)
* `sqlite3` gem
* `fxruby` gem


Installation
------------
1. Install `ruby`. Note that even if your system requires you to
	install the system ruby (i.e. ruby 1.8.7 on Ubuntu 12.04) you
	can still use your `rvm` versions to build/run `fxruby` programs
	like this one.
2. install stuff you'll need to install the fx gui tool kit:
	<pre>
	sudo apt-get install g++ libxrandr-dev libfox-1.6-dev
	</pre>
3. install the `bundler` gem if you don't already have it:
	<pre>
	sudo gem install bundler
	</pre>
4. Get the source from github:
	<pre>
	git clone https://github.com/doctorme/time_tracker.git
	</pre>
5. Install the necessary gems:	
	<pre>
	cd time_tracker
	bundle install
	</pre>

Usage
-----
* Run the program:
	<pre>
	cd time_tracker
	ruby ./time_tracker.rb
	</pre>
* Make a new timesheet or open an existing one using the file menu or use the hotkeys: **Ctrl-N** and **Ctrl-O** respectively
* Click *Start* when you start work; click *Stop* when you stop.
* The timesheet is saved every time you exit or click *Stop*. You can also save the timesheet manually with **Ctrl-S** if you want to save it to a different file.

Notes
-----
* A timesheet is supposed to represent all the hours you worked on a particular project. So make new timesheets when you start new projects, and when going back to work on an existing project then open its corresponding timesheet.
* The *.timesheet* files are actually just sqlite3 database files. So if you're an SQL wizard or you have some other sqlite3 program that can do fancy stuff with the data then have at it :)

Motivation
-------
I don't really expect anyone to use this thing seriously except, perhaps, myself. The motivation here is just to learn more about Ruby outside of [Ruby on Rails](http://rubyonrails.org/). Specifically, I wanted to learn quick and easy GUI development with Ruby because I frequently write little scripts to do things around the office, some of them might be used some day by people who're allergic to the dreaded command line interface.

License
-------
This program and all its source code are covered by the **I just wanna share** license which is as follows:
	<pre>
	Do what you want with this program and its source code--
	I ain't gonna stop you-- but please give me credit for the
	code that I wrote.
	</pre>
