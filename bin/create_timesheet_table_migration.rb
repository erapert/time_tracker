require 'active_record'

class CreateTimesheetTable < ActiveRecord::Migration
	def gen_table
		create_table :timesheets do |t|
			# `id` is automatic
			t.datetime :start_time
			t.datetime :stop_time
		end
	end
end
