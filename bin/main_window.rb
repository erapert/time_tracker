require 'fox16'
include Fox

class MainWindow < FXMainWindow
	attr_reader :defaultTitle
	
	def initialize(fxapp, timesheetIO)
		@defaultTitle = 'Time Tracker'
		@io = timesheetIO
	
		super(fxapp, @defaultTitle, :opts => DECOR_ALL, :x => 100, :y => 100)
	
		@mainContainer = FXHorizontalFrame.new(
			self,
			FRAME_NONE | LAYOUT_CENTER_X | LAYOUT_CENTER_Y | PACK_UNIFORM_WIDTH,
			:padding => 20
		)
	
		@menuBar = FXMenuBar.new(self, LAYOUT_SIDE_TOP | LAYOUT_FILL_X)
		@fileMenu = FXMenuPane.new(self)
			FXMenuCommand.new(@fileMenu, "&New Timesheet\tCtl-N").connect(SEL_COMMAND, method(:onNewTimesheet))
			FXMenuCommand.new(@fileMenu, "&Open Timesheet\tCtl-O").connect(SEL_COMMAND, method(:onOpenTimesheet))
			FXMenuCommand.new(@fileMenu, "&Save Timesheet\tCtl-S").connect(SEL_COMMAND, method(:onSaveTimesheet))
			FXMenuCommand.new(@fileMenu, "&Close Timesheet\tCtl-W").connect(SEL_COMMAND, method(:onCloseTimesheet))
			FXMenuSeparator.new(@fileMenu)
			FXMenuCommand.new(@fileMenu, 'Write Timesheet Report').connect(SEL_COMMAND, method(:onWriteTimesheetReport))
			FXMenuSeparator.new(@fileMenu)
			FXMenuCommand.new(@fileMenu, "&Quit\tCtl-Q", nil, getApp(), FXApp::ID_QUIT)
			FXMenuTitle.new(@menuBar, "&File", nil, @fileMenu)
		FXHorizontalSeparator.new(self, LAYOUT_SIDE_TOP | SEPARATOR_GROOVE | LAYOUT_FILL_X)
	
		@startButton = FXButton.new(
			@mainContainer,
			'Start',
			:opts => FRAME_RAISED | FRAME_THICK | LAYOUT_CENTER_X | LAYOUT_FIX_WIDTH | LAYOUT_FIX_HEIGHT,
			:width => 300,
			:height => 200
		)
		@startButton.connect(SEL_COMMAND) do
			onClickedStart()
			@startButton.disable
			@stopButton.enable
		end
	
		@stopButton = FXButton.new(
			@mainContainer,
			'Stop',
			:opts => FRAME_RAISED | FRAME_THICK | LAYOUT_CENTER_X | LAYOUT_FIX_WIDTH | LAYOUT_FIX_HEIGHT,
			:width => 300,
			:height => 200
		)
		@stopButton.connect(SEL_COMMAND) do
			onClickedStop()
			@startButton.enable
			@stopButton.disable
		end
		
		# disable both buttons 'till you open a timesheet
		@startButton.disable
		@stopButton.disable
	end

	def create
		super
		show(PLACEMENT_SCREEN)
	end

	def onNewTimesheet(sender, sel, ptr)
		@dlg = FXFileDialog.new(self, 'New Timesheet')
		@dlg.selectMode = SELECTFILE_ANY # load/save to a file-- will create if not exists, use SELECTFILE_EXISTING to do open-only
		@dlg.patternList = ["All Files (*)", "Timesheets (*.timesheet)"]
		# if @ntsDlg.execute returns 0 then the user canceled out
		if @dlg.execute != 0
			makeTitle(File.basename(@dlg.filename))
			@io.create @dlg.filename
			@startButton.enable
		end
	end

	def onOpenTimesheet(sender, sel, ptr)
		@dlg = FXFileDialog.new(self, 'Open Timesheet')
		@dlg.selectMode = SELECTFILE_EXISTING
		@dlg.patternList = ["All Files (*)", "Timesheets (*.timesheet)"]
		if @dlg.execute != 0
			makeTitle(File.basename(@dlg.filename))
			@io.open @dlg.filename
			@startButton.enable
		end
	end

	def onSaveTimesheet(sender, sel, ptr)
		@io.save
	end

	def onCloseTimesheet(sender, sel, ptr)
		self.title = @defaultTitle
		@io.close
	end

	def onWriteTimesheetReport(sender, sel, ptr)
		@dlg = FXFileDialog.new(self, 'Write to file')
		@dlg.selectMode = SELECTFILE_ANY
		@dlg.patternList = ["All Files (*)"]
		if @dlg.execute != 0
			@io.write_report(@dlg.filename)
		end
	end

	def onClickedStart
		@io.start
	end

	def onClickedStop
		@io.stop
	end

	def makeTitle(str)
		self.title = "#{@defaultTitle} - #{str}"
	end
end
