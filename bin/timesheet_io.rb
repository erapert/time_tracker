require 'sqlite3'
require 'active_record'

require_relative './timesheets_model'
require_relative './create_timesheet_table_migration'

#
#	io for timesheet files
#
class TimesheetIO
	attr_reader :filename, :data, :extension
	
	public
		def initialize(timesheetFName = nil)
			open timesheetFName
			@extension = '.timesheet'
		end
	
		def open(fname)
			return false if !fname or !File.exists?(fname)
			puts 'io:open'
			begin
				dbConnect fname
			rescue => err
				puts err
				return false
			end
			@filename = fname
			return true
		end
		
		def close
			puts 'io:close'
			stop
			save
			dbDisconnect()
		end
		
		def create(fname)
			return false if !fname
			if !fname.end_with? @extension
				fname = fname + @extension
			end
			SQLite3::Database.new fname
			dbConnect fname
			CreateTimesheetTable.new.migrate :gen_table
			return true
		end
		
		def save
			puts 'io:save'
			@data.stop_and_save if @data
		end
		
		def start
			puts 'io:start'
			@data = Timesheets.new
			@data.set_start_now
		end
		
		def stop
			puts 'io:stop'
			if @data
				@data.set_stop_now
				@data.save
				@data = nil
			end
		end
		
		def write_report filename
			puts "io:write_report to '#{filename}'"
			allTimes = Timesheets.all
			
			fstr = "Timesheet:\nid, start_time, stop_time, duration"
			allTimes.each do |t|
				fstr += t.to_string + "\n"
			end
			
			File.write(filename, fstr)
		end
	
	private
		def dbConnect(timesheetFile)
			puts 'io:dbConnect()'
			if !ActiveRecord::Base.establish_connection(:adapter => 'sqlite3', :database => timesheetFile)
				puts "WARNING: failed to establish connection to #{timesheetFile}"
			end
		end
		
		def dbDisconnect
			puts 'io:dbDisconnect()'
			ActiveRecord::Base.clear_active_connections!
		end
end
