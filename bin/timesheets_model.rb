require 'active_record'

class Timesheets < ActiveRecord::Base
	
	TIME_MAGNITUDES = [:seconds, :minutes, :hours, :days]
	
	def set_start_now
		self.start_time = Time.now
	end
	
	def set_stop_now
		self.stop_time = Time.now
	end
	
	def stop_and_save
		self.stop_time = Time.now
		save
	end
	
	def to_string(magnitude = :hours)
		if !TIME_MAGNITUDES.include? magnitude
			raise "Invalid time magnitude: #{magnitude}"
		end
		
		return "#{id}, #{start_time}, #{stop_time}, #{time_diff(magnitude)} #{magnitude}"
	end
	
	# set the difference in hours
	# stop_time - start_time yields seconds
	def time_diff(magnitude = :hours)
		if !TIME_MAGNITUDES.include? magnitude
			raise "Invalid time magnitude: #{magnitude}"
		end
		
		return nil if stop_time == nil
		
		case magnitude
		when :seconds
			return (stop_time - start_time)
		when :minutes
			# seconds -> minutes
			return ((stop_time - start_time) / 60)
		when :hours
			# seconds -> minutes -> hours
			return (((stop_time - start_time) / 60) / 60)
		when :days
			# seconds -> minutes -> hours -> days
			return ((((stop_time - start_time) / 60) / 60) / 24)
		end
	end
end

