#!ruby

require_relative './bin/main_window'
require_relative './bin/timesheet_io'

if __FILE__ == $0
	app = FXApp.new 'APP TITLE', 'second thing'
	tsio = TimesheetIO.new
	
	MainWindow.new(app, tsio)
	app.create
	app.run
end
